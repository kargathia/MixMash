## Description

Selects a random option for each potion property.

Prints all descriptions, and their selected values.

Example output:
```
The potion is a draught
The potion causes schaffensfreude. Makes the enemies take damage as they deal it to the user.
The potion's strength is temporary but strong and wears off quickly.
The potion also causes (usually) temporary mood swings.
The potion is in a metal thermos.
The potion looks purple.
The potion has a constant heat.
The potion is airy and bubbly.
The potion smells like chicken.
The potion tastes like sand.
The potion has a label showing a title describing the exact opposite.
```

## Dependencies

* PyYaml: `pip install pyyaml`
* Python >= 3.4

## Usage

* `mixmash`
* `python3 mixmash.py`
* `mixmash -t mixmash.yaml`

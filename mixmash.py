#!/usr/bin/python3
"""
Iterates over a potion table, getting a random option for every description.
"""

import random
import argparse
import textwrap

try:
    import yaml
except ImportError:
    print('Unable to import yaml library.'
          ' You can use "pip install pyyaml" to install it')
    raise


def generate(props):
    """Selects a random option for each property description.

    Args:
        props (list): a list of property objects.
            Each property should be formatted as::

            {
                'descriptions': [
                    'description1',
                    'description2
                ],
                'options': [
                    'option1',
                    'option2',
                    'option3
                ]
            }

    Returns:
        list: all descriptions matched with a random argument.

    Example output::

        [
            'description1 option3',
            'description2 option1'
        ]
    """
    generated = []
    for prop in props:
        for desc in prop['descriptions']:
            options = prop['options']
            generated.append('{} {}.'.format(desc, random.choice(options)))
    return generated


def load_yaml(fname):
    with open(fname) as f:
        return yaml.load(f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generate a potion with random properties')
    parser.add_argument(
        '-t', '--table',
        metavar='YAML_FILE',
        help='potion property table. Defaults to mixmash.yaml',
        default='mixmash.yaml')
    args = parser.parse_args()

    print(*generate(load_yaml(args.table)), sep='\n')
